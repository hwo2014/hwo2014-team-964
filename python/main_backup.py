# Todo: Engine power
# Todo: Friction
# Todo: Drift up to 60 degree angle

import json
import socket
import sys
import math
import networkx

class Track(object):

    def __init__(self,data):
        self.id     = data['race']['track']['id']
        self.name   = data['race']['track']['name']
        self.lanes  = [x['distanceFromCenter'] for x in data['race']['track']['lanes']]
        self.cars   = [(x['id']['name'],x['id']['color']) for x in data['race']['cars']]
        self.laps   = data['race']['raceSession']['laps']
        self.pieces = []

        self.makeTrackPieces(data['race']['track']['pieces'],self.lanes)

    def __repr__(self):
        desc = str(self.laps) + " laps at " + self.name + " with " + str(len(self.lanes)) + " lanes with " + str(len(self.cars) - 1) + " opponents."
        return  desc

    def makeTrackPieces(self,piecedata,lanedata):
        # Create the pieces
        for piece in piecedata:
            tp = TrackPiece(piece,lanedata)
            self.pieces.append(tp)

        # Establish the neighbor pieces
        for idx,p in enumerate(self.pieces):
            if idx == 0:
                p.next = self.pieces[1]
                p.prev = self.pieces[len(self.pieces) - 1]
            elif idx == len(self.pieces) - 1:
                p.next = self.pieces[0]
                p.prev = self.pieces[idx - 1]
            else:
                p.next = self.pieces[idx + 1]
                p.prev = self.pieces[idx - 1]

            p.index = idx

        # Assign the best exit side
        for idx,p in enumerate(self.pieces):
            neighbor  = p.next

            dirs = {"Left":0,"Right":0,"None":0}
            for n in range(7):
                dirs[neighbor.bestEntry] += 1
                neighbor = neighbor.next

            # If the corner is followed primarily by straight,
            # exit on the side opposite the turn, if possible.
            if dirs["None"] > dirs["Left"] and dirs["None"] > dirs["Right"]:
                if p.bestEntry == "Right":
                    direction = "Left"
                else:
                    direction = "Right"

            # Otherwise, count and exit on the best side for 
            # upcoming corners
            elif dirs["Left"] > dirs["Right"]:
                direction = "Left"
            else:
                direction = "Right"

            p.bestExit = direction

    def countRemainingCurvesFrom(self,fromIndex):

        count = 0
        for n in range(fromIndex,len(self.pieces)):
            if self.pieces[n].curved is True:
                count += 1
        return count


class TrackPiece(object):

    def __init__(self,data,lanedata):
        self.length    = 0
        self.switch    = False
        self.radius    = 0
        self.angle     = 0
        self.bridge    = False
        self.next      = None
        self.prev      = None
        self.bestEntry = "None"
        self.bestExit  = None  # Exit the turn on this side of the track
        self.index     = -1
        self.curved    = False

        if data.get('length'):
            self.length = data.get('length')

        if data.get('switch'):
            self.switch = True

        if data.get('radius'):
            self.radius = data.get('radius')

        if data.get('angle'):
            self.curved = True
            self.angle  = data.get('angle')
            abs_angle   = self.angle
            if abs_angle < 0:
                abs_angle *= -1
            self.length = (2.0 * self.radius) * 3.14159265359 * (abs_angle / 360.0)
            if self.angle < 0:
                self.bestEntry = "Left"
            elif self.angle > 0:
                self.bestEntry = "Right"

        if data.get('bridge'):
            self.bridge = True

# Tells the bot what to do
class Driver(object):

    def __init__(self,bot,track):
        self.bot           = bot
        self.track         = track
        self.lastPiece     = None
        self.position      = None
        self.lastPiece     = None
        self.currentPiece  = None
        self.nextPiece     = None
        self.lapCount      = 1
        self.lastLap       = False
        self.firstLap      = True
        self.lapCurvesLeft = 0
        self.floorIt       = True
        self.currentAngle  = 0
        self.lastThrottle  = 0

    def updatePosition(self,currentPieceIndex,data):

        self.position     = currentPieceIndex
        self.currentPiece = self.track.pieces[currentPieceIndex]
        self.lastPiece    = self.currentPiece.prev
        self.nextPiece    = self.currentPiece.next

        for car in data:
            if car['id']['name'] == 'GameAid':
                self.currentAngle = car['angle']
                break

        if self.lapCount == self.track.laps:
            self.lastLap = True

        # Be a little riskier on the last lap? Eventually this will 
        # depend on position in the race.
        if self.lastLap is True:
            self.lapCurvesLeft = self.track.countRemainingCurvesFrom(self.position)

        if self.lastLap is True and self.lapCurvesLeft == 0:
            self.floorIt = True

        if self.lapCount > 1:
            self.firstLap = False

        # Gun it at the beginning
        if self.firstLap is True and self.position > 0:
            self.floorIt  = False


    def attemptLaneSwitch(self):

        if self.currentPiece.switch is True or self.nextPiece.switch is True: 
            direction = self.currentPiece.bestExit
            if direction is not None:
                self.bot.switchDirection(direction)
                # print "Moving to the",direction

    def adjustThrottle(self,data):
        throttleValue = 0.465
        # self.bot.throttle(0.465)

        # Straightaway
        if self.nextPiece.curved is False and self.nextPiece.next.curved is False:
            throttleValue = 0.6
            # self.bot.throttle(0.6)

        # Tight curves
        #tot_angle = abs(self.currentPiece.angle)
        tot_angle  = abs(self.nextPiece.angle)
        tot_angle += abs(self.nextPiece.next.angle)
        #tot_angle += abs(self.nextPiece.next.next.angle)

        if tot_angle > 80:
            #print('slowing for curves')
            throttleValue = 0.2
            # self.bot.throttle(0.2)


        # Riskier on the last lap
        if self.floorIt is True:
            if self.lastLap is True:
                throttleValue = 1.0
                # self.bot.throttle(1.0)

            # Speed boost off the line
            if self.firstLap is True:
                throttleValue = 0.5
                # self.bot.throttle(0.5)

        if abs(self.currentAngle) > 30:
            print "Angle too large!"
            throttleValue = 0.2
            # self.bot.throttle(0.2)

        if abs(self.currentAngle) > 40:
            print "Angle way too large!"
            throttleValue = 0.1
            # self.bot.throttle(0.1)

        self.bot.throttle(throttleValue)
        self.lastThrottle = throttleValue



# The vehicle
class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket       = socket
        self.name         = name
        self.key          = key
        self.color        = ""

    def on_yourCar(self,data):
        print(data['name'] + " is color " + data['color'])
        self.color = data['color']

    def on_gameInit(self,data):
        self.track = Track(data)
        print self.track
        self.driver = Driver(self,self.track)

    def msg(self, msg_type, data):
        themessage = json.dumps({"msgType": msg_type, "data": data})
        self.send(themessage)

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        # self.raceSelf("keimola")
        self.raceAlone("germany")

        # germany
        # Keimola

    def raceAlone(self,track):
        return self.msg("createRace",{
            "botId":{"name": self.name, "key": self.key},
            "trackName":track,
            "carCount": 1
            })

    def raceSelf(self,track):
        return self.msg("joinRace",{
            "botId":{"name": self.name, "key":"9PcCedRrP8BVAg"},
            "trackName":track,"carCount":2
            })

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def switchDirection(self,direction_to_turn):
        self.msg("switchLane",direction_to_turn)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        trackPieceIdx = self.get_trackpiece_index(data)
        self.driver.updatePosition(trackPieceIdx,data)

        self.driver.attemptLaneSwitch()
        self.driver.adjustThrottle(data)
        # self.throttle(0.65) #self.currentPiece.maxThrottle) 0.656

    def get_trackpiece_index(self,data):
        for car in data:
            if car['id']['name'] == self.name:
                return car['piecePosition']['pieceIndex']

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_lapFinish(self,data):
        self.driver.lapCount += 1
        print data['car']['name'] + " (" + data['car']['color'] + ") lapped in " + str(data['lapTime']['millis']/1000.0)

    def on_game_end(self, data):
        print("Race ended")
        self.ping()


    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'yourCar': self.on_yourCar,
            'gameInit': self.on_gameInit,
            'lapFinished': self.on_lapFinish,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
