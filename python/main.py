import json
import socket
import sys

# This also imports Pathfinder and NetworkX
from track import Track, Timer
from piece import Piece

class Driver(object):
    def __init__(self,_bot):
        self.bot                = _bot
        self.current_piece      = 0
        self.next_piece         = 1
        self.current_start_lane = -1
        self.current_end_lane   = -1
        self.lap                = 0
        self.lastLap            = False
        self.inPieceDistance    = 0
        self.angle              = 0
        self.track              = self.bot.track
        self.switchGuide        = self.track.switchGuide
        self.switchDirection    = "Right"
        self.switchedInPiece    = -1
        self.preswitches        = [n-1 for n in self.track.switches]
        self.turboAvailable     = False
        self.turboCooldown      = False
        self.lastPositionData   = None
        self.starting           = True

        self.timer              = Timer(self,self.track)

        self.defaultThrottle    = 0.5
        self.lastThrottle       = 0
        self.throttleInfo       = ""

        if self.track.trackid == "germany":
            self.defaultThrottle = 0.45
        elif self.track.trackid == "usa":
            self.defaultThrottle = 0.87
        elif self.track.trackid == "keimola":
            self.defaultThrottle = 0.65

    def updatePosition(self,data):
        for car in data:
            if car['id']['name'] == self.bot.name:
                self.current_piece      = car['piecePosition']['pieceIndex']
                self.current_start_lane = car['piecePosition']['lane']['startLaneIndex']
                self.current_end_lane   = car['piecePosition']['lane']['endLaneIndex']
                self.lap                = car['piecePosition']['lap']
                self.inPieceDistance    = car['piecePosition']['inPieceDistance']
                self.angle              = car['angle']

                break # Not currently interested in the other cars

        self.next_piece = self.current_piece + 1
        if self.next_piece > len(self.switchGuide) - 1:
            self.next_piece = 0


        if self.track.isQualifying is True:
            self.lastLap = False
        else:
            # Indicate that we are on the last lap
            if self.lap == self.track.laps - 1:
                self.lastLap = True

        self.lastPositionData = data

    def switchLanes(self):
        if self.current_piece in self.preswitches and self.current_piece != self.switchedInPiece:
            self.switchedInPiece = self.current_piece
            bestLane = self.switchGuide[self.next_piece][1]
            if bestLane == 0:
                self.switchDirection = "Left"
            else:
                self.switchDirection = "Right"

            # print "Activating switch to the",self.switchDirection,"in piece",self.current_piece
            self.bot.switchDirection(self.switchDirection)

    def onStraightaway(self,length):
        # Check the 5 pieces coming after this one to see if they are straight
        thispiece = self.track.pieces[self.current_piece]
        for n in range(length):
            thispiece = thispiece.next
            if thispiece.curved is True and abs(thispiece.angle) > 22.5:
                return False
        thispiece = self.track.pieces[self.current_piece]
        if thispiece.angle == 0:
            return True
        else:
            # Coming out of a corner
            if self.inPieceDistance > (thispiece.lengths[self.current_end_lane] / 5.0):
                return True
            else:
                return False

    def enteringCorner(self,length):
        thispiece = self.track.pieces[self.current_piece]
        for n in range(length):
            thispiece = thispiece.next
            if thispiece.angle > 0:
                return True
        return False

    def inCorner(self):
        if self.track.pieces[self.current_piece].curved is True:
            return True
        else:
            return False

    def applyTurbo(self):

        # Turbo for the last straightaway on the last lap
        if self.turboAvailable is True:
            if self.onStraightaway(4) is True:
                if self.lastLap is True:
                    if self.current_piece > len(self.track.pieces) * 0.8:
                        if abs(self.angle) < 50:
                            print "Last lap turbo!"
                            self.turboAvailable = False
                            self.bot.useTurbo()

    def applyThrottle(self):
        if self.starting is True:
            self.throttleInfo = "Starting fast!"
            self.lastThrottle = 1.0
            self.bot.throttle(self.lastThrottle)

            # Achieved target speed off start line. Cancel starting push.
            if self.timer.this_velocity > 5.5:
                self.starting = False

        elif self.turboCooldown is False:
            self.applyTurbo()

            # Straightaway
            if self.onStraightaway(4) is True:
                self.throttleInfo = "onStraightaway(4)"
                self.lastThrottle = 1.0

            # Entering a Corner 
            elif self.enteringCorner(2) is True:
                if self.timer.this_velocity > self.defaultThrottle * 10:
                    self.throttleInfo = "enteringCorner(2) too fast"
                    self.lastThrottle = self.defaultThrottle * 0.5
                else:
                    self.throttleInfo = "enteringCorner(2) proper speed"
                    self.lastThrottle = self.defaultThrottle
            else:
                # In a corner
                if self.inCorner() is True:
                    if abs(self.angle) > 50:
                        self.throttleInfo = "car's angle too high!"
                        self.lastThrottle = 0

                    elif self.timer.this_velocity > self.defaultThrottle * 9.5:
                        self.throttleInfo = "cornering too fast!"
                        self.lastThrottle = 0

                    elif abs(self.angle) < 10:
                        self.throttleInfo = "cornering too slowly!"
                        self.lastThrottle = 1.0
                    else:
                        self.throttleInfo = "in corner at reasonable speed"
                        self.lastThrottle = self.defaultThrottle

                else:
                    self.throttleInfo = "default throttle"
                    self.lastThrottle = self.defaultThrottle
            self.bot.throttle(self.lastThrottle)
        else:
            if self.timer.this_velocity > 7.5:
                self.throttleInfo = "turboCooldown with high velocity"
                self.lastThrottle = self.defaultThrottle / 2.0
                self.bot.throttle(self.lastThrottle)
            else:
                self.throttleInfo = "turboCooldown"
                self.turboCooldown = False
                self.lastThrottle = self.defaultThrottle
                self.bot.throttle(self.lastThrottle)

        self.timer.updateTimer(self.lastPositionData)


class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket        = socket
        self.name          = name
        self.key           = key
        self.color         = ''
        self.trackLocation = ''
        self.starting      = True
        self.track         = None
        self.driver        = None

    def on_yourCar(self,data):
        self.color = data['color']

    def on_gameInit(self,data):
        print "gameInit"
        self.track = Track(data)
        print self.track
        self.driver = Driver(self)

    def msg(self, msg_type, data):
        themessage = json.dumps({"msgType": msg_type, "data": data})
        self.send(themessage)

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        self.trackLocation = "keimola"
        #self.raceSelf("germany")
        #self.raceAlone("germany")
        # self.raceAlone("keimola")
        # self.raceAlone("usa")

        #self.raceAlone(self.trackLocation)


        self.officialRace();

        # germany
        # Keimola

    def officialRace(self):
        return self.msg("join",{
            "name": self.name, 
            "key":  self.key
            })

    def raceAlone(self,track):
        return self.msg("createRace",{
            "botId":{"name": self.name, "key": self.key},
            "trackName":track,
            "carCount": 1
            })

    def raceSelf(self,track):
        return self.msg("joinRace",{
            "botId":{"name": self.name, "key":"9PcCedRrP8BVAg"},
            "trackName":track,"carCount":6
            })

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def switchDirection(self,direction_to_turn):
        self.msg("switchLane",direction_to_turn)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.driver.switchLanes()
        self.throttle(1.0)
        #self.driver.applyThrottle()

    def on_car_positions(self, data):
        self.driver.updatePosition(data)
        self.driver.switchLanes()
        self.driver.applyThrottle()

    def on_turboAvailable(self,data):
        print "Turbo is available"
        self.driver.turboAvailable = True
        self.driver.timer.recordEvent("turboAvailable",str(data))
        self.ping()

    def on_turboStart(self,data):
        self.driver.timer.recordEvent("turbostart")
        self.ping()

    def on_turboEnd(self,data):
        print "Turbo ended." #,data
        self.driver.timer.recordEvent("turboend")
        self.driver.turboCooldown = True
        self.ping()

    def useTurbo(self):
        print "Activating Turbo!"
        self.msg("turbo","Varoom!")

    def on_lapFinished(self,data):
        #print "on_lapFinished:",data
        print "\n",data['car']['name'],"lapped in",data['lapTime']['millis']/1000.0,"\n"
        self.ping()

    def on_crash(self, data):
        if data['name'] == self.name:
            self.driver.timer.recordEvent("crash")
            print("Crashed!!")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.driver.timer.saveData()
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'yourCar': self.on_yourCar,
            'gameInit': self.on_gameInit,
            'turboAvailable': self.on_turboAvailable,
            'turboStart': self.on_turboStart,
            'turboEnd': self.on_turboEnd,
            'lapFinished': self.on_lapFinished,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
