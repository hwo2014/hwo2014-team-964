# Requires NetworkX
import networkx as nx

class PathFinder(object):
    def __init__(self,pieces,numLanes):
        self.graph = nx.DiGraph()
        self.switchIndices = []

        for idx in range(numLanes):
            for n in range(len(pieces)):
                thisnode = "P" + str(n)   + "_L" + str(idx)
                nextnode = "P" + str(n+1) + "_L" + str(idx)
                if n == len(pieces) - 1:
                    nextnode = "P0_L" + str(idx)
                
                self.graph.add_edge(thisnode,nextnode)

        # Now add edges between pieces where there is a switch
        for idx,piece in enumerate(pieces):
            if piece.switch is True:
                # print "Switch at piece index:",idx
                self.switchIndices.append(idx)
                # This piece has a switch, so add edges
                for n in range(numLanes - 1):
                    thisnode = "P" + str(idx) + "_L" + str(n)
                    nextnode = "P" + str(idx) + "_L" + str(n + 1)
                    self.graph.add_edge(thisnode,nextnode)
                    self.graph.add_edge(nextnode,thisnode)

        # Assign weights to edges
        for e in self.graph.edges():
            piece_num_start = self.pieceNum(e[0])
            piece_num_end   = self.pieceNum(e[1])

            # This is a switch, so the weight is 1 to make unnecessary switching more expensive
            if piece_num_end == piece_num_start:
                self.graph.edge[e[0]][e[1]]['weight'] = 1

            # Regular edge on the graph
            else:
                lane_num = self.laneNum(e[0])
                self.graph.edge[e[0]][e[1]]['weight'] = pieces[piece_num_start].lengthOfLane(lane_num)

        # Create the dummy edges
        for n in range(numLanes):
            startnode = "P0_L" + str(n)
            self.graph.add_edge("START",startnode,weight = 0)
            endnode   = "P" + str(len(pieces) - 1) + "_L" + str(n)
            self.graph.add_edge(endnode,"END")

        # Get the best path
        self.bestPath = nx.shortest_path(self.graph,"START","END",'weight')

        # Trim the dummy nodes off of it
        self.bestPath = self.bestPath[1:-1]
        
        # Iterate through the best path and remove the dummy switch nodes
        self.fixedPath = []
        for idx,n in enumerate(self.bestPath):
            if idx == 0:
                self.fixedPath.append(n)
                continue
            thispiece = self.pieceNum(n)
            lastpiece = self.pieceNum(self.bestPath[idx-1])
            if thispiece == lastpiece:
                continue
            else:
                self.fixedPath.append(n)

        # Now pull out just the numbers of the lanes
        # Potential TODO: handle skipping of multiple lanes
        self.bestLanes = []
        for p in self.fixedPath:
            self.bestLanes.append(self.laneNum(p))

    def pieceNum(self,nodename):
        return int(nodename.split("_")[0][1:])

    def laneNum(self,nodename):
        return int(nodename.split("_")[1][1:])

    @property
    def bestRoute(self):
        return self.bestLanes