from piece import Piece

# This also imports NetworkX to find the best route
from pathfinder import *

class Track(object):
    def __init__(self,data):
        self.trackdata = data
        self.lanes     = [x['distanceFromCenter'] for x in data['race']['track']['lanes']]
        self.trackname = data['race']['track']['name']
        self.trackid   = data['race']['track']['id']
        self.isQualifying = False
        try:
            self.laps       = data['race']['raceSession']['laps']
            self.durationMs = -1
        except:
            self.laps  = 0
            self.durationMs = data['race']['raceSession']['durationMs']
            self.isQualifying = True
        self.teams     = [(car['id']['name'],car['id']['color']) for car in data['race']['cars']]
        self.pieces    = [Piece(x,self) for x in data['race']['track']['pieces']]

        # Establish links between pieces for easier calling
        for idx,p in enumerate(self.pieces):
            p.index = idx
            if idx == 0:
                p.prev = self.pieces[-1]
            else:
                p.prev = self.pieces[idx - 1]
            if idx == len(self.pieces) - 1:
                p.next = self.pieces[0]
            else:
                p.next = self.pieces[idx + 1]

        # The following deterines the proper path and then creates 
        # a guide that the Driver can follow to determine whether to
        # attempt to switch lanes to the optimal route

        self.pathfinder = PathFinder(self.pieces,len(self.lanes))
        self.bestRoute  = self.pathfinder.bestRoute
        self.switches   = self.pathfinder.switchIndices

        self.switchGuide = []
        for idx,r in enumerate(self.bestRoute):
            containsSwitch = False
            if idx in self.switches:
                containsSwitch = True
            self.switchGuide.append([containsSwitch,r])

        for idx,item in enumerate(self.switchGuide):
            next = idx + 1
            if idx == len(self.switchGuide) - 1:
                next = 0
            if item[0] is True:
                self.switchGuide[idx][1] = self.switchGuide[next][1]

        #for idx,piece in enumerate(self.pieces):
        #    print idx, piece.angle

        # for n in self.switches:
        #     for x in range(n):
        #         self.switchGuide[x][1] = self.switchGuide[n][1]
        # for n in range(self.switches[-1],len(self.switchGuide) - 1):
        #     self.switchGuide[n][1] = self.switchGuide[0][1]


    def __repr__(self):
        if self.durationMs > 0:
            val = str(self.durationMs) + " millisecons at " + self.trackname + " with "
        else:
            val = str(self.laps) + " laps at " + self.trackname + " with "
        for car in self.teams:
            val += car[0] + "(" + car[1] + "), "
        val = val[:-2]
        return val

class Timer(object):
    def __init__(self,driver,track):
        self.driver             = driver
        self.track              = track
        self.last_piece         = None
        self.last_piece_length  = 0
        self.last_piece_idx     = 0
        self.last_lane          = 0
        self.last_dist_in_piece = 0
        self.last_velocity      = 0
        self.this_velocity      = 0
        self.this_piece         = None
        self.this_piece_length  = 0
        self.this_piece_idx     = 0
        self.this_lane          = 0
        self.this_dist_in_piece = 0
        self.header             = ['lap','piece','lane','throttle','throttle info','velocity','trackAngle','carAngle','info']
        self.data               = []
        self.data.append(",".join(self.header))

    def recordEvent(self,event="event",data=""):
        eventrec = ""
        for n in range(len(self.header)):
            eventrec = eventrec + event + ","

        eventrec = eventrec + data
        self.data.append(eventrec)

    def saveData(self):
        print "Saving Data..."
        f = open(self.driver.bot.name + '_data.csv','w')
        for l in self.data:
            f.write(l)
            f.write("\n")
        f.close()

    def updateTimer(self,data):
        self.last_piece_idx     = self.this_piece_idx
        self.this_piece_idx     = self.driver.current_piece

        self.last_piece         = self.this_piece
        self.this_piece         = self.track.pieces[self.this_piece_idx]

        self.last_lane          = self.this_lane
        self.this_lane          = self.driver.current_start_lane

        self.last_piece_length  = self.this_piece_length
        self.this_piece_length  = self.this_piece.lengths[self.this_lane]

        self.last_dist_in_piece = self.this_dist_in_piece
        self.this_dist_in_piece = self.driver.inPieceDistance

        self.last_velocity      = self.this_velocity

        # Calculate velocity
        if self.last_piece_idx == self.this_piece_idx:
            # We're in the same piece, take the dist_in_piece differential
            self.this_velocity = self.this_dist_in_piece - self.last_dist_in_piece
        else:
            self.this_velocity = self.this_dist_in_piece + (self.last_piece_length - self.last_dist_in_piece)

        d = [self.driver.lap,self.this_piece_idx,self.this_lane,self.driver.lastThrottle,self.driver.throttleInfo,self.this_velocity,self.this_piece.angle,self.driver.angle]
        d = [str(x) for x in d]
        d = ",".join(d)
        self.data.append(d)