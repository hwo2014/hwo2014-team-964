import json
import socket
import sys
import networkx as nx
from collections import deque

class TrackGraphMaker(object):
    def __init__(self,data):
        # Using a NetworkX MultiDiGraph, as described here:
        # http://networkx.lanl.gov/reference/classes.multidigraph.html
        self.T              = nx.DiGraph()

        # Stores the distace from center for each lane. The index of the value
        # in the list is equal to the index of the lane on the track
        self.lanes          = [x['distanceFromCenter'] for x in data['race']['track']['lanes']]
        self.lane_offsets_r = [(x*-1) for x in self.lanes]
        self.lane_offsets_l = [x      for x in self.lanes] # redundant, but here for consistency

        # Get the representation of the pieces into a list
        self.pieces         = [x for x in data['race']['track']['pieces']]

        # Store each track piece as a node - 1 node for each piece for each lane
        # Nodes are in the form PxLy where P means Piece and L means Lane
        for idx,lane in enumerate(self.lanes):
            for n in range(len(self.pieces)):
                thisnode = "P" + str(n)   + "_L" + str(idx)
                nextnode = "P" + str(n+1) + "_L" + str(idx)
                if n == len(self.pieces) - 1:
                    nextnode = "P0_L" + str(idx)
                
                self.T.add_edge(thisnode,nextnode)

        # Now add edges between pieces where there is a switch
        for idx,piece in enumerate(self.pieces):
            if piece.get("switch"):
                print "Switch at piece index:",idx
                # This piece has a switch, so add edges
                for n in range(len(self.lanes) - 1):
                    thisnode = "P" + str(idx) + "_L" + str(n)
                    nextnode = "P" + str(idx) + "_L" + str(n + 1)
                    self.T.add_edge(thisnode,nextnode)
                    self.T.add_edge(nextnode,thisnode)

        # Add properties to nodes. First, we find the track piece and the
        # lane number. We'll need both to assign the length of the piece.
        for n in self.T.nodes():
            nodeparts   = n.split("_")
            piecenumber = int(nodeparts[0][1:])
            lanenumber  = int(nodeparts[1][1:])
            # print "Setting props for piece:",piecenumber,"at lane:",lanenumber
            piece_properties = self.pieces[piecenumber]

            self.T.node[n]['piece'] = piecenumber
            self.T.node[n]['lane']  = lanenumber

            # Is it a starting piece?
            if piecenumber == 0:
                self.T.node[n]['start'] = True
            else:
                self.T.node[n]['start'] = False

            # Is it a final piece?
            if piecenumber == len(self.pieces) - 1:
                self.T.node[n]['finish'] = True
            else:
                self.T.node[n]['finish'] = False

            # If it is a straightaway, get the length
            if piece_properties.get('length'):
                self.T.node[n]['length'] = piece_properties.get('length')

            # Does it contain a switch?
            if piece_properties.get('switch'):
                self.T.node[n]['switch'] = True
            else:
                self.T.node[n]['switch'] = False

            # If it is a corner, what is the radius?
            if piece_properties.get('radius'):
                self.T.node[n]['radius'] = piece_properties.get('radius')
            else:
                self.T.node[n]['radius'] = 0

            # If it is a corner, calculate the distance around it
            if piece_properties.get('angle'):
                self.T.node[n]['angle']  = piece_properties.get('angle')
                self.T.node[n]['curved'] = True

                ang       = piece_properties.get('angle')
                abs_angle = abs(ang)

                lane_distance_mod = 0 #abs(self.lanes[lanenumber])

                # For right hand turns, subtract the abs(lane distance mod)
                # from the radius for the right hand lane

                # Subtract the lane_distance_mod from the radius for 
                # right-hand turns 
                if ang > 0:
                    lane_distance_mod = self.lane_offsets_r[lanenumber]
                elif ang < 0:
                    lane_distance_mod = self.lane_offsets_l[lanenumber]

                self.T.node[n]['length'] = (2.0 * (piece_properties.get('radius') + lane_distance_mod)) * 3.14159265359 * (abs_angle / 360.0)
                
                if piece_properties.get('angle') < 0:
                    self.T.node[n]['bestEntry'] = "Left"
                elif piece_properties.get('angle') > 0:
                    self.T.node[n]['bestEntry'] = "Right"
            else:
                self.T.node[n]['angle']     = 0
                self.T.node[n]['curved']    = False
                self.T.node[n]['bestEntry'] = "None"

            # Is it a bridge?
            if piece_properties.get('bridge'):
                self.T.node[n]['bridge'] = True
            else:
                self.T.node[n]['bridge'] = False

        # Now we need to assign weights to the edges based on
        # the length of the beginning node
        for e in self.T.edges():
            start_data = e[0].split("_")
            start_pos  = int(start_data[0][1:])

            end_data   = e[1].split("_")
            end_pos    = int(end_data[0][1:])

            # This is a switch edge - leave blank?
            if start_pos == end_pos:
                self.T.edge[e[0]][e[1]]['weight'] = 0
                continue

            # Assign to the edge weight the length of the beginning node
            self.T.edge[e[0]][e[1]]['weight'] = self.T.node[e[0]]['length']

        # Add dummy nodes that represent the general position. Since we
        # know where we are starting but do not know where we will end,
        # this allows us to specify a dummy node as the end of a route,
        # and we then trim it off of the path returned.
        for n in self.T.nodes():
            thisn  = n.split("_")[0]
            dummyn = thisn
            self.T.add_edge(n,dummyn)
            self.T.edge[n][dummyn]['weight'] = 0 

        # Add START to represent the general starting node, that has
        # edges that go to each of the nodes in Position 0
        for n in range(len(self.lanes)):
            endnode = "P0_L" + str(n)
            self.T.add_edge("START",endnode,weight = 0)


    def getGraph(self):
        return self.T


class Track(object):
    def __init__(self,data):
        self.trackdata = data

        # Using a NetworkX MultiDiGraph, as described here:
        # http://networkx.lanl.gov/reference/classes.multidigraph.html
        graphmaker = TrackGraphMaker(data)
        self.graph = graphmaker.getGraph()

        # Stores the distace from center for each lane. The index of the value
        # in the list is equal to the index of the lane on the track
        self.lanes     = [x['distanceFromCenter'] for x in data['race']['track']['lanes']]

        # Get the representation of the pieces into a list
        self.pieces       = [x for x in data['race']['track']['pieces']]
        self.startNode    = "START"
        self.finishNode   = "P" + str(len(self.pieces) - 1)
        self.outside_lane = 0

    def nodeForPosition(self,position,lane):
        key = "P" + str(position) + "_L" + str(lane)
        return self.graph.node[key]

    def shortestPath(self,startposition,startlane,endposition = "",endlane = ""):
        startnode = "P" + str(startposition) + "_L" + str(startlane)

        if endposition == "" and endlane == "":
            endnode   = self.finishNode
        else:
            endnode   = "P" + str(endposition) + "_L" + str(endlane)

        return nx.shortest_path(self.graph,startnode,endnode,'weight')

    def absoluteShortestPath(self,startpiece="",endpiece=""):
        if endpiece == "":
            endpiece = self.finishNode

        if startpiece == "":
            startpiece = self.startNode

        path = nx.shortest_path(self.graph,startpiece,endpiece,'weight')
        del path[0]
        del path[-1]
        return path


    @property
    def inside_lane(self):
        return len(self.lanes) - 1

    def getInitialPlans(self):
        return self.absoluteShortestPath()

        # plans = {}
        # for n in range(len(self.lanes)):
        #     p = self.shortestPath(0,n)
        #     p.pop() # Get rid of the dummy at the end
        #     plans[n] = p
        # return plans

    def nodeEntry(self,node):
        return self.graph.node[node]['bestEntry']

    def printNode(self,node):
        pos  = self.graph.node[node]['piece']
        lane = self.graph.node[node]['lane']
        ent  = self.graph.node[node]['bestEntry']
        ang  = self.graph.node[node]['angle']
        swt  = self.graph.node[node]['switch']
        rad  = self.graph.node[node]['radius']

        print "pos:",pos,"l:",lane,"enter:",ent,"ang:",ang,"switch:",swt,"radius:",rad

    def switchIndices(self,nodes):
        indices = []
        for idx,n in enumerate(nodes):
            if self.graph.node[n]['switch'] == True:
                indices.append(idx)
        return indices

    def isStraightaway(self,nodes):
        for n in nodes:
            if self.graph.node[n]['curved'] is True:
                return False

        return True

    def length(self,thenode):
        return self.graph.node[thenode]['length']
            


class Driver(object):
    def __init__(self,data,bot,track,racingTrack):
        self.bot                  = bot
        self.name                 = self.bot.name
        self.track                = track
        self.starting_node        = "P"
        # self.initial_plans        = self.track.getInitialPlans()
        self.current_plan         = self.track.getInitialPlans()
        self.plan_distances       = []
        self.steering_plan        = []
        self.current_position     = 0
        self.last_position        = 0
        self.current_start_lane   = 0
        self.current_end_lane     = 0
        self.switch_direction     = "None"
        self.position_initialized = False
        self.need_to_update_plan  = False
        self.angle                = 0
        self.lap                  = 0
        self.inPieceDistance      = 0
        self.racingTrack          = racingTrack
        self.defaultSpeed         = 0.5
        self.turboAvailable       = False
        self.turboActive          = False
        self.turboEnded           = True
        self.lastDistance         = 0
        self.totalDistance        = 0
        self.piecesTraversed      = []
        self.distancesTraveled    = []
        self.lastInPieceDist      = 0
        self.velocity             = 0
        self.exportData           = [['piece','piece_name','piece_length','lastInPieceDist','inPieceDistance','velocity','totalDistance']]

        if self.racingTrack == "usa":
            self.defaultSpeed = 0.85
        elif self.racingTrack == "keimola":
            self.defaultSpeed = 0.64
        elif self.racingTrack == "germany":
            self.defaultSpeed = 0.45

    def updatePosition(self,data):
        for car in data:
            if car['id']['name'] == self.name:
                self.current_position   = car['piecePosition']['pieceIndex']
                self.current_start_lane = car['piecePosition']['lane']['startLaneIndex']
                self.current_end_lane   = car['piecePosition']['lane']['endLaneIndex']
                self.lap                = car['piecePosition']['lap']
                self.inPieceDistance    = car['piecePosition']['inPieceDistance']
                self.angle              = car['angle']

                if self.position_initialized is False or self.need_to_update_plan is True:
                    if self.position_initialized is False:
                        # self.current_plan   = self.initial_plans[self.current_start_lane]
                        self.plan_distances = [self.track.length(x) for x in self.current_plan]
                        self.deDupCurrentPlan()
                        self.position_initialized = True
                    self.updatePlan(data)

                self.calculateTimes(data)

                # Track speed and distances
                if thisPosition != self.last_position:
                    self.piecesTraversed.append(self.current_plan[self.last_position])
                    self.distancesTraveled.append(self.plan_distances[self.last_position])
                    self.velocity = self.inPieceDistance + (self.plan_distances[self.last_position] - self.lastInPieceDist)
                    print "velocity = ",self.
                    self.last_position = thisPosition
                else:
                    self.velocity = self.inPieceDistance - self.lastInPieceDist
                    

                self.lastDistance  = self.totalDistance
                self.totalDistance = sum(self.distancesTraveled) + self.inPieceDistance

                # For exporting data
                d = []
                d.append(self.current_position)
                d.append(self.current_plan[self.current_position])
                d.append(self.plan_distances[self.current_position])
                d.append(self.lastInPieceDist)
                d.append(self.inPieceDistance)
                d.append(self.velocity)
                d.append(self.totalDistance)
                self.exportData.append(d)

                self.lastInPieceDist = self.inPieceDistance

                # Probably not needed
                self.updateSwitchPosition()

    def calculateTimes(self,data):
        if self.current_position == self.last_position:
            self.velocity = self.inPieceDistance - self.lastInPieceDist
        else:
            self.velocity = self.inPieceDistance + ()

    def decideWhatToDo(self,data):
        if self.steering_plan[self.current_position] is not "None":
            self.bot.switchDirection(self.steering_plan[self.current_position])
        
        if self.turboAvailable is False:
            # implement dynamic speed
            self.bot.throttle(self.defaultSpeed)
        else:
            if self.enteringStraightaway is True:
                self.bot.useTurbo()
            else:
                self.bot.throttle(self.defaultSpeed)

    def exportDataNow(self):
        f = open("run_data.csv","w")
        for line in self.exportData:
            d = [str(x) for x in line]
            l = ",".join(d)
            f.write(l)
            f.write("\n")
        f.close()

    def updatePlan(self,data):
        # TODO Create a new plan
        self.need_to_update_plan = False
        self.starting_node       = self.current_plan[0]
        self.updateSteeringPlan()

        #for n in self.current_plan:
        #    print self.track.printNode(n)

        #print self.current_plan

    def getLane(self,node):
        return int(node.split("_")[1][1:])

    def getPos(self,node):
        return int(node.split("_")[0][1:])

    @property
    def enteringStraightaway(self):
        # Current and next 3 sections must be straight
        sections = []
        nodes    = []
        #sections.append(self.current_position)
        x = self.current_position
        for n in range(1,6):
            x = x + 1
            if x == len(self.current_plan):
                x = 0
            sections.append(x)

        nodes = [self.current_plan[y] for y in sections]
        isStraight = self.track.isStraightaway(nodes)
        return isStraight


    def updateSteeringPlan(self):
        # Problem here is that we need a full lap to normalize
        self.steering_plan = []
        lanes = []
        for p in self.current_plan:
            nodelane = self.getLane(p)
            lanes.append(nodelane)
            if nodelane > 0:
                self.steering_plan.append("Right")
            else:
                self.steering_plan.append("Left")

        switches = self.track.switchIndices(self.current_plan)

        for n in switches:
            o = n+1
            if o == len(self.steering_plan):
                o = 0
            self.steering_plan[n] = self.steering_plan[o]

        for idx,n in enumerate(self.steering_plan):
            if idx in switches:
                continue
            if idx+1 in switches:
                self.steering_plan[idx] = self.steering_plan[idx+1]
                continue
            self.steering_plan[idx] = "None"

        print self.steering_plan



    def updateSwitchPosition(self):
        x = self.current_position + 1
        
        if x >= len(self.steering_plan):
            x = 0

        self.switch_direction = self.steering_plan[x]

    def deDupCurrentPlan(self):
        # Must already have a current plan to use this...
        newplan = []
        for idx,p in enumerate(self.current_plan):
            if idx == 0:
                newplan.append(p)
                continue
            lastpos = newplan[-1].split("_")[0]
            thispos = self.current_plan[idx    ].split("_")[0]

            if thispos == lastpos:
                continue
            else:
                newplan.append(p)

        self.current_plan = newplan



class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket       = socket
        self.name         = name
        self.key          = key
        self.color        = ''
        self.onTrack      = ''
        self.driver       = None
        self.starting     = True

    def on_yourCar(self,data):
        self.color = data['color']

    def on_gameInit(self,data):
        self.track = Track(data)
        self.driver = Driver(data,self,self.track,self.onTrack)

    def msg(self, msg_type, data):
        themessage = json.dumps({"msgType": msg_type, "data": data})
        self.send(themessage)

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        self.onTrack = "keimola"
        # self.raceSelf("keimola")
        # self.raceAlone("germany")
        # self.raceAlone("keimola")
        # self.raceAlone("usa")
        self.raceAlone(self.onTrack)
        # self.officialRace();

        # germany
        # Keimola

    def officialRace(self):
        return self.msg("join",{
            "name": self.name, 
            "key":  self.key
            })

    def raceAlone(self,track):
        return self.msg("createRace",{
            "botId":{"name": self.name, "key": self.key},
            "trackName":track,
            "carCount": 1
            })

    def raceSelf(self,track):
        return self.msg("joinRace",{
            "botId":{"name": self.name, "key":"9PcCedRrP8BVAg"},
            "trackName":track,"carCount":2
            })

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def switchDirection(self,direction_to_turn):
        self.msg("switchLane",direction_to_turn)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")

        # Safe starting throttle
        self.throttle(0.5)

    def on_car_positions(self, data):
        if self.starting is True:
            self.throttle(0.7)
            self.starting = False
        self.driver.updatePosition(data)
        self.driver.decideWhatToDo(data)

    def on_turboAvailable(self,data):
        # self.driver.turboAvailable = True
        print "turbo available "

    def on_turboStart(self,data):
        self.driver.turboActive    = True
        self.driver.turboEnded     = False
        self.driver.turboAvailable = False
        print "on_turboStart:" #,data

    def on_turboEnd(self,data):
        self.driver.turboEnded  = True
        print "on_turboEnd:" #,data

    def useTurbo(self):
        self.msg("turbo","Varoom!")

    def on_lapFinished(self,data):
        print "on_lapFinished:" #,data

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()
        self.driver.exportDataNow()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'yourCar': self.on_yourCar,
            'gameInit': self.on_gameInit,
            'turboAvailable': self.on_turboAvailable,
            'turboStart': self.on_turboStart,
            'turboEnd': self.on_turboEnd,
            'lapFinished': self.on_lapFinished,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
