class Piece(object):
    def __init__(self,data,_track):
        self.track     = _track
        self.lengths   = []
        self.switch    = False
        self.radius    = 0
        self.angle     = 0
        self.next      = None
        self.prev      = None
        self.enterOn   = "None"
        self.index     = -1
        self.curved    = False
        self.bestLane  = -1

        if data.get('length'):
            num = len(self.track.lanes)
            for n in range(num):
                self.lengths.append(data.get('length'))

        if data.get('switch'):
            self.switch = True

        if data.get('radius'):
            self.radius = data.get('radius')
            self.curved = True

        if data.get('angle'):
            self.angle  = data.get('angle')
            self.calculateCurveLengths(self.track.lanes)

    def calculateCurveLengths(self,offsets):
        abs_angle   = abs(self.angle)
        # For right-hand turns, the lane offset will be subtracted from the radius
        # and for left-hand turns, the lane offset is added to the radius.
        for lane in offsets:
            # Left hand turns
            if self.angle < 0:
                length = (2.0 * (self.radius + lane)) *  3.14159265359 * (abs_angle / 360.0)
                self.lengths.append(length)
            # Right hand turns
            elif self.angle > 0:
                length = (2.0 * (self.radius - lane)) *  3.14159265359 * (abs_angle / 360.0)
                self.lengths.append(length)

        if self.angle < 0:
            self.enterOn = "Left"
        elif self.angle > 0:
            self.enterOn = "Right"

        if self.angle > 0:
            self.bestLane = 0
        else:
            self.bestLane = len(offsets) - 1

    def lengthOfLane(self,laneIndex):
        return self.lengths[laneIndex]